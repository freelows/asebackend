﻿using Asesoramiento.Aplicacion.Comun.Automapper;
using AutoMapper;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asesoramiento.Web.Infraestructura.Automapper
{
    public class MapperProvider
    {
        private readonly Container _container;

        public MapperProvider(Container container)
        {
            _container = container;
        }

        public IMapper GetMapper()
        {
            return new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
                cfg.ConstructServicesUsing(_container.GetInstance);
            }));
        }
    }
}