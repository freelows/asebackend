﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;
using Asesoramiento.Aplicacion.Comun.Exceptions;
using Newtonsoft.Json;

namespace Asesoramiento.Web.Infraestructura
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        public override Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            var code = HttpStatusCode.InternalServerError;
            string result;

            if (context.Exception is ValidationException)
            {
                var valException = context.Exception as ValidationException;
                code = HttpStatusCode.BadRequest;
                result = JsonConvert.SerializeObject(valException.Failures);
            }
            else
            {
                result = JsonConvert.SerializeObject(new { error = context.Exception.ToString()});
            }

            var response = context.Request.CreateResponse(code, new { error = result });
            response.Headers.Add("X-Error", "Error");
            context.Result = new ResponseMessageResult(response);
            return base.HandleAsync(context, cancellationToken);
        }
    }
}