﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using Asesoramiento.Aplicacion.Comun.Comportamientos;
using Asesoramiento.Aplicacion.Funciones.Financiero.Commands.CrearCliente;
using Asesoramiento.Aplicacion.Interfaces;
using Asesoramiento.Persistencia;
using Asesoramiento.Web.Infraestructura;
using Asesoramiento.Web.Infraestructura.Di;
using Asesoramiento.Web.Infraestructura.Filtros;
using Asesoramiento.Web.Infraestructura.Seguridad;
using FluentValidation;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Owin;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using System.Web.Routing;
using Asesoramiento.Web.App_Start;

[assembly: OwinStartup(typeof(Asesoramiento.Web.Startup))]

namespace Asesoramiento.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalFilters.Filters.Add(new ClientActionFilterAttribute());
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            app.Use(async (context, next) =>
            {
                using (AsyncScopedLifestyle.BeginScope(container))
                {
                    await next();
                }
            });

            container.Register<IGenerateDbContext, GenerateAppDbContext>(Lifestyle.Scoped);
            container.Register<IValidator<CrearClienteCommand>, CrearClienteCommandValidator>();

            var assemblies = new[] { typeof(ValidateNada<>).Assembly };
            container.Collection.Register(typeof(IValidator<>), assemblies);
            container.RegisterSingleton(() => GetMapper(container));
            container.BuildMediator(typeof(CrearClienteCommandHandler).GetTypeInfo().Assembly);
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);


            app.UseCors(CorsOptions.AllowAll);
            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),//token expiration time  
                Provider = new DotNetTechyAuthServerProvider()
            };

            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);//register the request  
        }


        private AutoMapper.IMapper GetMapper(Container container)
        {
            var mp = container.GetInstance<Infraestructura.Automapper.MapperProvider>();
            return mp.GetMapper();
        }
    }
}
