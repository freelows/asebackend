﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Asesoramiento.Aplicacion.Funciones.Asesoramientos.Financieros.Commands.Crear;
using Asesoramiento.Aplicacion.Funciones.Asesoramientos.Financieros.Queries.Listar;
using MediatR;

namespace Asesoramiento.Web.Controllers
{
    [Authorize]
    public class FinancierosController : ApiController
    {
        private readonly IMediator _mediator;

        public FinancierosController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<ItemAsesoramientoFinanciero>> Get()
        {
            return await _mediator.Send(new ListarQuery());
        }

        [HttpPost]
        public async Task<int> Post(CrearCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}
