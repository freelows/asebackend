﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Asesoramiento.Aplicacion.Funciones.Usuarios.Queries.Obtener;
using MediatR;

namespace Asesoramiento.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/Usuarios")]
    public class UsuariosController : ApiController
    {
        private readonly IMediator _mediator;

        public UsuariosController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("info/{userName}")]
        public async Task<UsuarioViewModel> Get(string userName)
        {
            return await _mediator.Send(new ObtenerUsuarioQuery { UserName = userName });
        }


        [HttpGet]
        [Route("quiensoy")]
        public async Task<UsuarioViewModel> QuienSoy()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return await _mediator.Send(new ObtenerUsuarioQuery { UserName = identity.Name });
        }

    }
}
