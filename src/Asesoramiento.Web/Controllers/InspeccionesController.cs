﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Asesoramiento.Aplicacion.Funciones.Inspecciones.Commands.Crear;
using Asesoramiento.Aplicacion.Funciones.Inspecciones.Queries.Listar;
using MediatR;

namespace Asesoramiento.Web.Controllers
{
    [Authorize]
    public class InspeccionesController : ApiController
    {
        private readonly IMediator _mediator;

        public InspeccionesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<ItemInspeccion>> Get()
        {
            return await _mediator.Send(new ListarQuery());
        }

        [HttpPost]
        public async Task<int> Post(CrearCommand command)
        {
            var identity = (ClaimsIdentity)User.Identity;
            command.CreadoPor = identity.Name;
            return await _mediator.Send(command);
        }
    }
}
