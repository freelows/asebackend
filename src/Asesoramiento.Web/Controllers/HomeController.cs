﻿using Asesoramiento.Aplicacion.Funciones.Financiero.Queries.BuscarCliente;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace Asesoramiento.Web.Controllers
{
    //[Authorize(Roles = "Ejecutivo")]
    public class HomeController : ApiController
    {
        private readonly IMediator _mediator;
        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<ClienteViewModel> Get(string rut)
        {
            var identity = (ClaimsIdentity)User.Identity;

            var result = await _mediator.Send(new BuscarClienteQuery { Rut = rut });

            result.Nombres = result.Nombres + " " + identity.Name;

            return result;
        }
    }
}