﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Modelo.Entidades
{
   public class EntradaSeguimiento
    {
        public int Id { get; set; }
        public int? SolicitudInspeccionId { get; set; }
        public SolicitudInspeccion SolicitudInspeccion { get; set; }
        public string Detalle { get; set; }
        public DateTime Fecha { get; set; }
    }
}
