﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Modelo.Entidades
{
   public class SolicitudInspeccion
    {
        public SolicitudInspeccion()
        {
            Fecha = DateTime.Now;
        }

        public int Id { get; set; }

        public int? ClienteId { get; set; }
        public Cliente Cliente { get; set; }
        public string Estado { get; set; }

        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Patente { get; set; }
        public int? Anio { get; set; }
        public string UbicacionVehiculo { get; set; }

        public string TelefonoContacto { get; set; }
        public string NombreContacto { get; set; }
        public string CorreoContacto { get; set; }

        public string CreadoPor { get; set; }
        public DateTime Fecha { get; set; }

    }
}
