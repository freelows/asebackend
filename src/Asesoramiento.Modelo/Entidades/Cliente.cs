﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Modelo.Entidades
{
    public class Cliente
    {
        public int Id { get; set; }
        public int? Rut { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public string Telefono { get; set; }
        public string Correo { get; set; }
    }
}
