﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Modelo.Entidades
{
   public  class SolicitudAsesoramientoFinanciero
    {
        public int Id { get; set; }
        public int? ClienteId { get; set; }
        public Cliente Cliente { get; set; }

        public int? MontoAuto { get; set; }
        public int?  Pie { get; set; }
        public int? IngresoLiquido { get; set; }
        public int? Plazo { get; set; }

        public string BancoFinanciamientio { get; set; }

    }
}
