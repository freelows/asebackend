﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Modelo.Entidades
{
    public class EntradaDocumento
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Estado { get; set; }

        public int? SolicitudInspeccionId { get; set; }
        public SolicitudInspeccion SolicitudInspeccion { get; set; }

        public int? ArchivoId { get; set; }
        public Archivo Archivo { get; set; }
    }
}
