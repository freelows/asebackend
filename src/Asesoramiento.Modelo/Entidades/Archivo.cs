﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Modelo.Entidades
{
    public class Archivo
    {
        public int Id { get; set; }
        public byte[] Data { get; set; }
        public string Ext { get; set; }
        public string Nombre { get; set; }
    }
}
