﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Comun.Helpers
{
    public class RutHelper
    {
        public static string DigitoVerificador(Int32 rut)
        {
            double T = rut;
            double M = 0, S = 1;
            while (T != 0)
            {
                S = (S + T % 10 * (9 - M++ % 6)) % 11;
                T = Math.Floor(T / 10);
            }
            string dv = S != 0 ? (S - 1).ToString() : "K";
            return dv;
        }

        public static string DigitoVerificador(string rut)
        {
            if (rut.EndsWith("k") || rut.EndsWith("K"))
                return "K";

            var rutLimpio = rut.Split('-').First().Replace(".", "");
            int rutValor = 0;
            if (int.TryParse(rutLimpio, out rutValor))
            {
                if (rutValor < 99999999)
                    return DigitoVerificador(rutValor);
                int rutValor2 = 0;
                if (int.TryParse(rutLimpio.Substring(0, rutLimpio.Length - 1), out rutValor2))
                    return DigitoVerificador(rutValor2);
            }

            return string.Empty;
        }

        public static string Rut(int rut)
        {
            return $"{Formatear(rut.ToString())}-{DigitoVerificador(rut)}";
        }

        public static string Rut(int? rut)
        {
            if (rut.HasValue)
                return Rut(rut.Value);
            return string.Empty;
        }

        public static bool RutFormateadoEsValido(string rut)
        {
            rut = rut?.ToUpper();
            return string.IsNullOrEmpty(rut) || rut.EndsWith(DigitoVerificador(ExtraerRutDeRutFormateado(rut).GetValueOrDefault()));
        }

        public static int? ExtraerRutDeRutFormateado(string rutFormateado)
        {
            if (string.IsNullOrEmpty(rutFormateado)) return null;

            rutFormateado = rutFormateado.ToUpper();
            rutFormateado = rutFormateado.Replace(",", "");
            rutFormateado = rutFormateado.Replace(".", "");

            if (rutFormateado.Contains("-"))
            {
                rutFormateado = rutFormateado.Replace("-", "");
                rutFormateado = rutFormateado.Substring(0, rutFormateado.Length - 1);
            }

            int resultado = 0;
            if (int.TryParse(rutFormateado, out resultado))
                return resultado;

            return null;
        }

        public static string Formatear(string rut)
        {
            int cont = 0;
            var format = string.Empty;
            if (rut.Length != 0)
            {
                for (int i = rut.Length - 1; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "." + format;
                        cont = 0;
                    }
                }
            }
            return format;
        }

    }
}
