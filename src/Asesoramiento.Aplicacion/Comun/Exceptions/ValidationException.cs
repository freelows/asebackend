﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace Asesoramiento.Aplicacion.Comun.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException()
            : base("One or more validation failures have occurred.")
        {
            Failures = new List<string>();
        }

        public ValidationException(List<ValidationFailure> failures)
            : this()
        {
            var propertyNames = failures
                .Select(e => e.PropertyName)
                .Distinct();

            foreach (var propertyName in propertyNames)
            {
                var propertyFailures = failures
                    .Where(e => e.PropertyName == propertyName)
                    .Select(e => e.ErrorMessage)
                    .ToArray();

                var propertyFailuresMeda = failures
                    .Where(e => e.PropertyName == propertyName);

                Failures.Add($"{propertyFailuresMeda.First().FormattedMessagePlaceholderValues["PropertyName"]} {string.Join(",", propertyFailures)}");
            }
        }

        public List<string> Failures { get; }
    }
}
