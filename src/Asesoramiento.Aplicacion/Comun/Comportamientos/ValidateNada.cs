﻿using FluentValidation;

namespace Asesoramiento.Aplicacion.Comun.Comportamientos
{
    public sealed class ValidateNada<T> : AbstractValidator<T>
    {
    }
}
