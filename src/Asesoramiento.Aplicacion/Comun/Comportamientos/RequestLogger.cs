﻿using MediatR.Pipeline;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Comun.Comportamientos
{
    public class RequestLogger<TRequest> : IRequestPreProcessor<TRequest>
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public Task Process(TRequest request, CancellationToken cancellationToken)
        {
            Logger.Info("Request: {request} ", request);
            return Task.CompletedTask;
        }
    }
}
