﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Asesoramiento.Aplicacion.Comun.Automapper
{
    public interface IMapFrom<T>
    {
        void Mapping(Profile profile);
    }
}
