﻿using Asesoramiento.Modelo;
using Asesoramiento.Modelo.Entidades;
using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;


namespace Asesoramiento.Aplicacion.Interfaces
{
    public interface IAppDbContext : IDisposable
    {
        DbSet<Usuario> Usuarios { get; set; }
        DbSet<Cliente> Clientes { get; set; }

        DbSet<SolicitudAsesoramientoFinanciero> SolicitudesAseshoramiento { get; set; }
        DbSet<SolicitudInspeccion> SolicitudesInspeccion { get; set; }
        DbSet<EntradaDocumento> EntradasDocumento { get; set; }
        DbSet<EntradaSeguimiento> EntradasSeguimiento { get; set; }
        DbSet<Archivo> Archivos { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
