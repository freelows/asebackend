﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Interfaces
{
    public interface IGenerateDbContext
    {
        IAppDbContext GenerateNewContext();
    }
}
