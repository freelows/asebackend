﻿using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using Asesoramiento.Aplicacion.Interfaces;
using AutoMapper;
using MediatR;

namespace Asesoramiento.Aplicacion.Funciones.Usuarios.Queries.Obtener
{
    public class ObtenerUsuarioQueryHandler : IRequestHandler<ObtenerUsuarioQuery, UsuarioViewModel>
    {
        private readonly IMapper _mapper;
        private readonly IGenerateDbContext _context;
        public ObtenerUsuarioQueryHandler(IGenerateDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UsuarioViewModel> Handle(ObtenerUsuarioQuery request, CancellationToken cancellationToken)
        {
            using (var db = _context.GenerateNewContext())
            {
                var criterio = request.UserName.ToLower();
                var usuario = await db.Usuarios.FirstOrDefaultAsync(t => t.UserName == criterio, cancellationToken);
                return new UsuarioViewModel
                {
                    Nombre = usuario.UserName,
                    Rol = usuario.Role,
                    AgregarFinanciero = true
                };
            }
        }
    }
}