﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MediatR;

namespace Asesoramiento.Aplicacion.Funciones.Usuarios.Queries.Obtener
{
    public class ObtenerUsuarioQuery : IRequest<UsuarioViewModel>
    {
        public string UserName { get; set; }
    }
}
