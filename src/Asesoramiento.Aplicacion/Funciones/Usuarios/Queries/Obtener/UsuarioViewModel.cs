﻿namespace Asesoramiento.Aplicacion.Funciones.Usuarios.Queries.Obtener
{
    public class UsuarioViewModel
    {
        public string Nombre { get; set; }
        public string Rol { get; set; }
        public bool AgregarFinanciero { get; set; }
        public bool AgregarInspeccion { get; set; }
    }
}