﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Funciones.Financiero.Commands.CrearCliente
{
   public class CrearClienteCommandValidator : AbstractValidator<CrearClienteCommand>
    {
        public CrearClienteCommandValidator()
        {
            RuleFor(x => x.Rut).NotEmpty().WithMessage("debe ser especificada");
        }
    }
}
