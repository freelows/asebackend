﻿using Asesoramiento.Aplicacion.Comun.Helpers;
using Asesoramiento.Aplicacion.Interfaces;
using Asesoramiento.Modelo.Entidades;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Funciones.Financiero.Commands.CrearCliente
{
    public class CrearClienteCommandHandler : IRequestHandler<CrearClienteCommand>
    {
        private readonly IGenerateDbContext _context;
        public CrearClienteCommandHandler(IGenerateDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(CrearClienteCommand request, CancellationToken cancellationToken)
        {
            using (var _db = _context.GenerateNewContext())
            {
                var cliente = new Cliente
                {
                    Rut = RutHelper.ExtraerRutDeRutFormateado(request.Rut),
                    Nombres = request.Nombres,
                    Apellidos = request.Apellidos
                };
                _db.Clientes.Add(cliente);
                await _db.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}
