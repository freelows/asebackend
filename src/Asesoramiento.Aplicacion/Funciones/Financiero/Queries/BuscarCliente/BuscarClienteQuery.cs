﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Funciones.Financiero.Queries.BuscarCliente
{
   public class BuscarClienteQuery : IRequest<ClienteViewModel>
    {
        public string Rut { get; set; }
    }
}
