﻿using Asesoramiento.Aplicacion.Comun.Automapper;
using Asesoramiento.Aplicacion.Comun.Helpers;
using Asesoramiento.Modelo.Entidades;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Funciones.Financiero.Queries.BuscarCliente
{
    public class ClienteViewModel : IMapFrom<Cliente>
    {
        public string Rut { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Cliente, ClienteViewModel>()
                .ForMember(dest => dest.Rut, opt => opt.MapFrom(src => src.Rut.HasValue ? RutHelper.Rut(src.Rut.Value) : string.Empty));
        }
    }
}
