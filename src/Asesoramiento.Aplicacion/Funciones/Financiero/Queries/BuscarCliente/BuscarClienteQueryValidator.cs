﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Funciones.Financiero.Queries.BuscarCliente
{
    public class BuscarClienteQueryValidator : AbstractValidator<BuscarClienteQuery>
    {
        public BuscarClienteQueryValidator()
        {
            RuleFor(x => x.Rut).NotEmpty().WithMessage("debe ser especificada");
        }
    }
}
