﻿using Asesoramiento.Aplicacion.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Funciones.Financiero.Queries.BuscarCliente
{
    public class BuscarClienteQueryHandler : IRequestHandler<BuscarClienteQuery, ClienteViewModel>
    {
        private readonly IGenerateDbContext _context;
        private readonly IMapper _mapper;
        public BuscarClienteQueryHandler(IGenerateDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ClienteViewModel> Handle(BuscarClienteQuery request, CancellationToken cancellationToken)
        {
            using (var _db = _context.GenerateNewContext())
            {
                var clientes = await _db.Clientes.ToListAsync(cancellationToken);
                var cliente = clientes[0];
                var result = _mapper.Map<ClienteViewModel>(cliente);
                return result;
            }
        }
    }
}
