﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asesoramiento.Aplicacion.Comun.Automapper;
using Asesoramiento.Aplicacion.Comun.Helpers;
using Asesoramiento.Modelo.Entidades;
using AutoMapper;
using MediatR;

namespace Asesoramiento.Aplicacion.Funciones.Asesoramientos.Financieros.Commands.Crear
{
    public class CrearCommand : IRequest<int>, IMapFrom<SolicitudAsesoramientoFinanciero>
    {
        public string Rut { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public int? MontoAuto { get; set; }
        public int? Pie { get; set; }
        public int? IngresoLiquido { get; set; }
        public int? Plazo { get; set; }
        public string BancoFinanciamientio { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CrearCommand, SolicitudAsesoramientoFinanciero>()
                .ForMember(dest => dest.Cliente, opt => opt.Ignore());

            profile.CreateMap<CrearCommand, Cliente>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Rut, opt => opt.MapFrom(src => RutHelper.ExtraerRutDeRutFormateado(src.Rut)));
        }
    }
}
