﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Asesoramiento.Aplicacion.Comun.Helpers;
using Asesoramiento.Aplicacion.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;

namespace Asesoramiento.Aplicacion.Funciones.Asesoramientos.Financieros.Queries.Listar
{
    public class ListarQueryHandler : IRequestHandler<ListarQuery, IEnumerable<ItemAsesoramientoFinanciero>>
    {
        private readonly IGenerateDbContext _gcontext;
        private readonly IMapper _mapper;
        public ListarQueryHandler(IGenerateDbContext gcontext, IMapper mapper)
        {
            _gcontext = gcontext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ItemAsesoramientoFinanciero>> Handle(ListarQuery request, CancellationToken cancellationToken)
        {
            using (var db = _gcontext.GenerateNewContext())
            {
                var data = await db.SolicitudesAseshoramiento.Include(t => t.Cliente).ToListAsync(cancellationToken);
                IEnumerable<ItemAsesoramientoFinanciero> result = _mapper.Map<IEnumerable<ItemAsesoramientoFinanciero>>(data);
                return result;
            }
        }
    }
}
