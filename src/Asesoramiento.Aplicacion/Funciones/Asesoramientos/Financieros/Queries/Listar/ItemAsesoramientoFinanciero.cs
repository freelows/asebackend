﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asesoramiento.Aplicacion.Comun.Automapper;
using Asesoramiento.Aplicacion.Comun.Helpers;
using Asesoramiento.Modelo.Entidades;
using AutoMapper;

namespace Asesoramiento.Aplicacion.Funciones.Asesoramientos.Financieros.Queries.Listar
{
    public class ItemAsesoramientoFinanciero : IMapFrom<SolicitudAsesoramientoFinanciero>
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public string Cliente { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public int? MontoAuto { get; set; }
        public int? Pie { get; set; }
        public int? IngresoLiquido { get; set; }
        public int? Plazo { get; set; }

        public string BancoFinanciamientio { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SolicitudAsesoramientoFinanciero, ItemAsesoramientoFinanciero>()
                .ForMember(dest => dest.Cliente, opt => opt.MapFrom(src => src.Cliente != null ? $"{src.Cliente.Nombres} {src.Cliente.Apellidos}" : string.Empty))
                .ForMember(dest => dest.Rut, opt => opt.MapFrom(src => src.Cliente != null ? RutHelper.Rut(src.Cliente.Rut) : string.Empty));
        }
    }
}
