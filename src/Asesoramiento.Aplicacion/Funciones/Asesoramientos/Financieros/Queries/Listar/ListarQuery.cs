﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace Asesoramiento.Aplicacion.Funciones.Asesoramientos.Financieros.Queries.Listar
{
    public class ListarQuery : IRequest<IEnumerable<ItemAsesoramientoFinanciero>>
    {
    }
}
