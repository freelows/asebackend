﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asesoramiento.Aplicacion.Funciones.Asesoramientos.Financieros.Queries.Listar
{
    public class ContenedorAsesoramientosFinancieros
    {
        public List<ItemAsesoramientoFinanciero> Items { get; set; }
        public int Cantidad { get; set; }
    }
}
