﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Asesoramiento.Aplicacion.Interfaces;
using Asesoramiento.Modelo.Entidades;
using AutoMapper;
using MediatR;

namespace Asesoramiento.Aplicacion.Funciones.Inspecciones.Commands.Crear
{
    public class CrearCommandHandler : IRequestHandler<CrearCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IGenerateDbContext _context;
        public CrearCommandHandler(IGenerateDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> Handle(CrearCommand request, CancellationToken cancellationToken)
        {
            using (var db = _context.GenerateNewContext())
            {
                var solicitud = _mapper.Map<SolicitudInspeccion>(request);
                solicitud.ClienteId = await HandleCliente(request, cancellationToken, db);
                var solicitudCreada = db.SolicitudesInspeccion.Add(solicitud);
                await db.SaveChangesAsync(cancellationToken);
                return solicitudCreada.Id;
            }
        }

        private async Task<int?> HandleCliente(CrearCommand request, CancellationToken cancellationToken, IAppDbContext db)
        {
            var cliente = _mapper.Map<Cliente>(request);

            if (!cliente.Rut.HasValue)
                return null;

            var clienteEncontrado = db.Clientes.FirstOrDefault(t => t.Rut == cliente.Rut);
            if (clienteEncontrado != null)
                return clienteEncontrado.Id;

            db.Clientes.Add(cliente);
            await db.SaveChangesAsync(cancellationToken);
            return cliente.Id;
        }
    }
}
