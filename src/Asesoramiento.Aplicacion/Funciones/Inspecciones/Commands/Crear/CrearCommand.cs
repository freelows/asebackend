﻿using Asesoramiento.Aplicacion.Comun.Automapper;
using Asesoramiento.Aplicacion.Comun.Helpers;
using Asesoramiento.Modelo.Entidades;
using AutoMapper;
using MediatR;

namespace Asesoramiento.Aplicacion.Funciones.Inspecciones.Commands.Crear
{
    public class CrearCommand : IRequest<int>, IMapFrom<SolicitudInspeccion>
    {
        public string Rut { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public string TelefonoContacto { get; set; }
        public string NombreContacto { get; set; }
        public string CorreoContacto { get; set; }
        public string CreadoPor { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Patente { get; set; }
        public int? Anio { get; set; }
        public string UbicacionVehiculo { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CrearCommand, SolicitudInspeccion>()
                .ForMember(dest => dest.Cliente, opt => opt.Ignore());

            profile.CreateMap<CrearCommand, Cliente>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Rut, opt => opt.MapFrom(src => RutHelper.ExtraerRutDeRutFormateado(src.Rut)));
        }
    }
}