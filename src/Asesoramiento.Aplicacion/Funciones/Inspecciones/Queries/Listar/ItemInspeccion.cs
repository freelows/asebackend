﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asesoramiento.Aplicacion.Comun.Automapper;
using Asesoramiento.Aplicacion.Comun.Helpers;
using Asesoramiento.Modelo.Entidades;
using AutoMapper;

namespace Asesoramiento.Aplicacion.Funciones.Inspecciones.Queries.Listar
{
    public class ItemInspeccion : IMapFrom<SolicitudInspeccion>
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public string Cliente { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Patente { get; set; }
        public int? Anio { get; set; }
        public string UbicacionVehiculo { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<SolicitudInspeccion, ItemInspeccion>()
                .ForMember(dest => dest.Cliente, opt => opt.MapFrom(src => src.Cliente != null ? $"{src.Cliente.Nombres} {src.Cliente.Apellidos}" : string.Empty))
                .ForMember(dest => dest.Rut, opt => opt.MapFrom(src => src.Cliente != null ? RutHelper.Rut(src.Cliente.Rut) : string.Empty));
        }
    }
}
