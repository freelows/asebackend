﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Asesoramiento.Aplicacion.Funciones.Asesoramientos.Financieros.Queries.Listar;
using Asesoramiento.Aplicacion.Interfaces;
using AutoMapper;
using MediatR;

namespace Asesoramiento.Aplicacion.Funciones.Inspecciones.Queries.Listar
{
    public class ListarQueryHandler : IRequestHandler<ListarQuery, IEnumerable<ItemInspeccion>>
    {
        private readonly IGenerateDbContext _gcontext;
        private readonly IMapper _mapper;
        public ListarQueryHandler(IGenerateDbContext gcontext, IMapper mapper)
        {
            _gcontext = gcontext;
            _mapper = mapper;
        }


        public async Task<IEnumerable<ItemInspeccion>> Handle(ListarQuery request, CancellationToken cancellationToken)
        {
            using (var db = _gcontext.GenerateNewContext())
            {
                var data = await db.SolicitudesInspeccion.Include(t => t.Cliente).ToListAsync(cancellationToken);
                IEnumerable<ItemInspeccion> result = _mapper.Map<IEnumerable<ItemInspeccion>>(data);
                return result;
            }
        }
    }
}
