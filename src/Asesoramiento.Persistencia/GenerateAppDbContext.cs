﻿using Asesoramiento.Aplicacion.Interfaces;

namespace Asesoramiento.Persistencia
{
    public class GenerateAppDbContext : IGenerateDbContext
    {
        public IAppDbContext GenerateNewContext()
        {
            IAppDbContext myDbContext = new AppDbContext();
            return myDbContext;
        }
    }
}
