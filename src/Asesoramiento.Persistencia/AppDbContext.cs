﻿using Asesoramiento.Aplicacion.Interfaces;
using Asesoramiento.Modelo.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Asesoramiento.Persistencia
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext() : base("AsesoriasContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }


        public virtual DbSet<SolicitudAsesoramientoFinanciero> SolicitudesAseshoramiento { get; set; }
        public virtual DbSet<SolicitudInspeccion> SolicitudesInspeccion { get; set; }
        public virtual DbSet<EntradaDocumento> EntradasDocumento { get; set; }
        public virtual DbSet<EntradaSeguimiento> EntradasSeguimiento { get; set; }
        public virtual DbSet<Archivo> Archivos { get; set; }

    }
}
