﻿namespace Asesoramiento.Persistencia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rut = c.Int(),
                        Nombres = c.String(),
                        Apellidos = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Clientes");
        }
    }
}
