﻿namespace Asesoramiento.Persistencia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigSolicitudes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "Telefono", c => c.String());
            AddColumn("dbo.Clientes", "Correo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clientes", "Correo");
            DropColumn("dbo.Clientes", "Telefono");
        }
    }
}
