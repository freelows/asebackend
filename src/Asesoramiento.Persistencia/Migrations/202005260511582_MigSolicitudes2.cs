﻿namespace Asesoramiento.Persistencia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigSolicitudes2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Archivoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Data = c.Binary(),
                        Ext = c.String(),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EntradaDocumentoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Estado = c.String(),
                        SolicitudInspeccionId = c.Int(),
                        ArchivoId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Archivoes", t => t.ArchivoId)
                .ForeignKey("dbo.SolicitudInspeccions", t => t.SolicitudInspeccionId)
                .Index(t => t.SolicitudInspeccionId)
                .Index(t => t.ArchivoId);
            
            CreateTable(
                "dbo.SolicitudInspeccions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClienteId = c.Int(),
                        Estado = c.String(),
                        Marca = c.String(),
                        Modelo = c.String(),
                        Patente = c.String(),
                        Anio = c.Int(),
                        UbicacionVehiculo = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clientes", t => t.ClienteId)
                .Index(t => t.ClienteId);
            
            CreateTable(
                "dbo.EntradaSeguimientoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SolicitudInspeccionId = c.Int(),
                        Detalle = c.String(),
                        Fecha = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SolicitudInspeccions", t => t.SolicitudInspeccionId)
                .Index(t => t.SolicitudInspeccionId);
            
            CreateTable(
                "dbo.SolicitudAsesoramientoFinancieroes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClienteId = c.Int(),
                        MontoAuto = c.Int(),
                        Pie = c.Int(),
                        IngresoLiquido = c.Int(),
                        Plazo = c.Int(),
                        BancoFinanciamientio = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clientes", t => t.ClienteId)
                .Index(t => t.ClienteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SolicitudAsesoramientoFinancieroes", "ClienteId", "dbo.Clientes");
            DropForeignKey("dbo.EntradaSeguimientoes", "SolicitudInspeccionId", "dbo.SolicitudInspeccions");
            DropForeignKey("dbo.EntradaDocumentoes", "SolicitudInspeccionId", "dbo.SolicitudInspeccions");
            DropForeignKey("dbo.SolicitudInspeccions", "ClienteId", "dbo.Clientes");
            DropForeignKey("dbo.EntradaDocumentoes", "ArchivoId", "dbo.Archivoes");
            DropIndex("dbo.SolicitudAsesoramientoFinancieroes", new[] { "ClienteId" });
            DropIndex("dbo.EntradaSeguimientoes", new[] { "SolicitudInspeccionId" });
            DropIndex("dbo.SolicitudInspeccions", new[] { "ClienteId" });
            DropIndex("dbo.EntradaDocumentoes", new[] { "ArchivoId" });
            DropIndex("dbo.EntradaDocumentoes", new[] { "SolicitudInspeccionId" });
            DropTable("dbo.SolicitudAsesoramientoFinancieroes");
            DropTable("dbo.EntradaSeguimientoes");
            DropTable("dbo.SolicitudInspeccions");
            DropTable("dbo.EntradaDocumentoes");
            DropTable("dbo.Archivoes");
        }
    }
}
