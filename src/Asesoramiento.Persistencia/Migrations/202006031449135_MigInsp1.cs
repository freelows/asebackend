﻿namespace Asesoramiento.Persistencia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigInsp1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SolicitudInspeccions", "TelefonoContacto", c => c.String());
            AddColumn("dbo.SolicitudInspeccions", "NombreContacto", c => c.String());
            AddColumn("dbo.SolicitudInspeccions", "CorreoContacto", c => c.String());
            AddColumn("dbo.SolicitudInspeccions", "CreadoPor", c => c.String());
            AddColumn("dbo.SolicitudInspeccions", "Fecha", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SolicitudInspeccions", "Fecha");
            DropColumn("dbo.SolicitudInspeccions", "CreadoPor");
            DropColumn("dbo.SolicitudInspeccions", "CorreoContacto");
            DropColumn("dbo.SolicitudInspeccions", "NombreContacto");
            DropColumn("dbo.SolicitudInspeccions", "TelefonoContacto");
        }
    }
}
