﻿using Asesoramiento.Aplicacion.Comun.Automapper;
using Asesoramiento.Aplicacion.Comun.Comportamientos;
using Asesoramiento.Aplicacion.Funciones.Financiero.Commands.CrearCliente;
using Asesoramiento.Aplicacion.Interfaces;
using Asesoramiento.Persistencia;
using AutoMapper;
using FluentValidation;
using MediatR;
using MediatR.Pipeline;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Asesoramiento.Aplicacion.Tests
{
    public class SimpleContainer
    {
        private static volatile Container _Container = null;
        private static void Init()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            container.Register<IGenerateDbContext, GenerateAppDbContext>(Lifestyle.Transient);

            container.Register<IValidator<CrearClienteCommand>, CrearClienteCommandValidator>();
            var assemblies = new[] { typeof(ValidateNada<>).Assembly };
            container.Collection.Register(typeof(IValidator<>), assemblies);
            container.RegisterSingleton(() => GetMapper(container));
            container.BuildMediator(typeof(CrearClienteCommandHandler).GetTypeInfo().Assembly);
            container.Verify();
            _Container = container;
        }

        public static Container Instance()
        {
            if (_Container == null)
            {
                lock (typeof(Container))
                {
                    if (_Container == null)
                    {
                        Init();
                    }
                }
            }
            return _Container;
        }
        public static Container Get()
        {
            return Instance();
        }

        protected static AutoMapper.IMapper GetMapper(Container container)
        {
            var mp = container.GetInstance<MapperProvider>();
            return mp.GetMapper();
        }

      
    }

    public class MapperProvider
    {
        private readonly Container _container;

        public MapperProvider(Container container)
        {
            _container = container;
        }

        public IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
                cfg.ConstructServicesUsing(_container.GetInstance);
            });
            IMapper mapper = new Mapper(config);
            return mapper;
        }
    }

    public static class ContainerExtension
    {
        public static Container BuildMediator(this Container container, params Assembly[] assemblies)
        {
            return BuildMediator(container, (IEnumerable<Assembly>)assemblies);
        }

        public static Container BuildMediator(this Container container, IEnumerable<Assembly> assemblies)
        {
            var allAssemblies = new List<Assembly> { typeof(IMediator).GetTypeInfo().Assembly };
            allAssemblies.AddRange(assemblies);

            container.RegisterSingleton<IMediator, Mediator>();
            container.Register(typeof(IRequestHandler<,>), allAssemblies);

            RegisterHandlers(container, typeof(IRequestExceptionAction<,>), allAssemblies.ToArray());
            RegisterHandlers(container, typeof(IRequestExceptionHandler<,,>), allAssemblies.ToArray());
            // we have to do this because by default, generic type definitions (such as the Constrained Notification Handler) won't be registered
            var notificationHandlerTypes = container.GetTypesToRegister(typeof(INotificationHandler<>), assemblies, new TypesToRegisterOptions
            {
                IncludeGenericTypeDefinitions = true,
                IncludeComposites = false,
            });
            container.Register(typeof(INotificationHandler<>), notificationHandlerTypes);

            container.Collection.Register(typeof(IPipelineBehavior<,>), new[]
            {
                typeof(RequestExceptionProcessorBehavior<,>),
                typeof(RequestExceptionActionProcessorBehavior<,>),
                typeof(RequestPreProcessorBehavior<,>),
                typeof(RequestPostProcessorBehavior<,>),
                typeof(RequestValidationBehavior<,>)
            });

            container.Collection.Register(typeof(IRequestPreProcessor<>), new[]
            {
                typeof(RequestLogger<>)
            });

            container.Collection.Register(typeof(IRequestPostProcessor<,>), Enumerable.Empty<Type>());

            container.Register(() => new ServiceFactory(container.GetInstance), Lifestyle.Singleton);

            return container;
        }

        private static void RegisterHandlers(Container container, Type collectionType, Assembly[] assemblies)
        {
            // we have to do this because by default, generic type definitions (such as the Constrained Notification Handler) won't be registered
            var handlerTypes = container.GetTypesToRegister(collectionType, assemblies, new TypesToRegisterOptions
            {
                IncludeGenericTypeDefinitions = true,
                IncludeComposites = false,
            });

            container.Collection.Register(collectionType, handlerTypes);
        }
    }
}
