﻿using System.Linq;
using System.Threading.Tasks;
using Asesoramiento.Aplicacion.Funciones.Inspecciones.Commands.Crear;
using Asesoramiento.Aplicacion.Interfaces;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;

namespace Asesoramiento.Aplicacion.Tests.Funciones.Inspecciones.Commands.Crear
{
    [TestClass()]
    public class CrearCommandHandlerTests
    {

        private readonly IMapper _mapper;
        private readonly IGenerateDbContext _db;

        public CrearCommandHandlerTests()
        {
            _db = SimpleContainer.Get().GetInstance<IGenerateDbContext>();
            _mapper = SimpleContainer.Get().GetInstance<IMapper>();
        }

        [TestMethod()]
        public async Task HandleTest()
        {
            var target = new CrearCommandHandler(_db, _mapper);
            var mock = new CrearCommand()
            {
                Rut = "12.581.156-6",
                Nombres = "Raul",
                Apellidos = "Ruiz Diaz",
                Marca = "Hyundai",
                Modelo = "Tucson",
                Patente = "BDP278",
                Anio = 2018,
                UbicacionVehiculo = "Mi casa"
            };
            var result = await target.Handle(mock, new System.Threading.CancellationToken());

            using (var realdb = _db.GenerateNewContext())
            {
                var entidad = realdb.SolicitudesInspeccion.Include(t => t.Cliente).Single(t => t.Id == result);
                Assert.AreEqual(entidad.Cliente.Rut.Value, 12581156);
                Assert.AreEqual(entidad.Cliente.Nombres, mock.Nombres);
                Assert.AreEqual(entidad.Cliente.Apellidos, mock.Apellidos);
                Assert.AreEqual(entidad.Marca, mock.Marca);
                Assert.AreEqual(entidad.Modelo, mock.Modelo);
                Assert.AreEqual(entidad.Patente, mock.Patente);
                Assert.AreEqual(entidad.Anio, mock.Anio);
                Assert.AreEqual(entidad.UbicacionVehiculo, mock.UbicacionVehiculo);
            }

        }
    }
}