﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Asesoramiento.Aplicacion.Funciones.Asesoramientos.Financieros.Commands.Crear;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Asesoramiento.Aplicacion.Interfaces;
using Asesoramiento.Modelo.Entidades;
using System.Data.Entity;

namespace Asesoramiento.Aplicacion.Tests.Funciones.Asesoramientos.Financieros.Commands.Crear
{
    [TestClass]
    public class CrearCommandHandlerTests
    {
        private readonly IMapper _mapper;
        private readonly IGenerateDbContext _db;

        public CrearCommandHandlerTests()
        {
            _db = SimpleContainer.Get().GetInstance<IGenerateDbContext>();
            _mapper = SimpleContainer.Get().GetInstance<IMapper>();
        }

        [TestMethod]
        public async Task HandleTest()
        {
            var target = new CrearCommandHandler(_db, _mapper);
            var mock = new CrearCommand
            {
                Rut = "12.581.156-6",
                Nombres = "Raul",
                Apellidos = "Ruiz Diaz",
                BancoFinanciamientio = "Banco 1",
                MontoAuto = 12000000,
                Pie = 2000000,
                IngresoLiquido = 1150000,
                Plazo = 24
            };

            var result = await target.Handle(mock, new System.Threading.CancellationToken());

            using (var realdb = _db.GenerateNewContext())
            {
                var entidad = realdb.SolicitudesAseshoramiento.Include(t => t.Cliente).Single(t => t.Id == result);
                Assert.AreEqual(entidad.Cliente.Rut.Value, 12581156);
                Assert.AreEqual(entidad.Cliente.Nombres, mock.Nombres);
                Assert.AreEqual(entidad.Cliente.Apellidos, mock.Apellidos);
                Assert.AreEqual(entidad.Plazo, mock.Plazo);
                Assert.AreEqual(entidad.Pie, mock.Pie);
                Assert.AreEqual(entidad.IngresoLiquido, mock.IngresoLiquido);
                Assert.AreEqual(entidad.MontoAuto, mock.MontoAuto);
                Assert.AreEqual(entidad.BancoFinanciamientio, mock.BancoFinanciamientio);
            }
        }
    }
}