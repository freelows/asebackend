﻿using System.Threading;
using System.Threading.Tasks;
using Asesoramiento.Aplicacion.Funciones.Usuarios.Queries.Obtener;
using Asesoramiento.Aplicacion.Interfaces;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Asesoramiento.Aplicacion.Tests.Funciones.Usuarios.Queries.Obtener
{
    [TestClass()]
    public class ObtenerUsuarioQueryHandlerTests
    {

        private readonly IMapper _mapper;
        private readonly IGenerateDbContext _db;

        public ObtenerUsuarioQueryHandlerTests()
        {
            _db = SimpleContainer.Get().GetInstance<IGenerateDbContext>();
            _mapper = SimpleContainer.Get().GetInstance<IMapper>();
        }

        [TestMethod()]
        public async Task HandleTest()
        {
           var target = new ObtenerUsuarioQueryHandler(_db, _mapper);

           var result = await target.Handle(new ObtenerUsuarioQuery {UserName = "admin1"}, new CancellationToken());

           Assert.IsTrue(result.Nombre == "admin1");
        }
    }
}